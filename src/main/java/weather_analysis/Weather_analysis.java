package weather_analysis;

/*
 * Authors Ben and Joakim
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Weather_analysis {
	static ArrayList<Measurment> measurementList = new ArrayList<Measurment>();

	public static void startAnalysis() {
		FileReader fr = null;
		try {
			fr = new FileReader("weather.json"); // Loads JSON file
		} catch (FileNotFoundException e1) {
			System.out.println("File(weather.json) not Found");
			System.exit(1);
		}
		JSONParser jp = new JSONParser();
		JSONObject jo = null;
		try {
			jo = (JSONObject) jp.parse(fr); // Parse as JSON formatted
		} catch (IOException e1) {
			System.out.println("Can't read file.");
			System.exit(2);
		} catch (ParseException e1) {
			System.out.println("Not a JSON file.");
			System.exit(3);
		}
		JSONArray ja = null;

		ja = (JSONArray) jo.get("measurements");
		if (ja == null) {
			System.out.println("File does not contain \"measurements\"");
			System.exit(4);
		}
		for (int i = 0; i < ja.size(); i++) // builds arrayList of all values
											// from the JSON file
		{
			JSONObject measurementObject = (JSONObject) ja.get(i);
			String time = measurementObject.get("time").toString();
			String[] timestampArr = time.split(" ");
			String[] dateArr = timestampArr[1].split("-");
			int year = Integer.parseInt(dateArr[0]);
			int month = Integer.parseInt(dateArr[1]);
			float temp = Float.parseFloat(measurementObject.get("temp").toString());
			float wind = Float.parseFloat(measurementObject.get("wind").toString());
			measurementList.add(new Measurment(time, year, month, temp, wind)); // makes
																				// a
																				// measurement
																				// object
																				// and
																				// adds
																				// it
																				// to
																				// the
																				// list
		}
		Weather_analysis.analysisMenu(); // Starts menu
	}

	public static ArrayList<Measurment> getList() {
		return measurementList;
	}

	public static void analysisMenu() { // menu starts here
		boolean run = true;
		System.out.println("Welcome to weather statistics software!\n");
		while (run) { // Runs until input is 5

			System.out.println("Select from following choices what you want to do:");
			System.out.println("1. Min-, Max-, Average Temperature for your json.");
			System.out.println("2. The highest measured wind force and it's time.");
			System.out.println("3. Average temperature per year for each year.");
			System.out.println("4. Standard deviation for your selected year.");
			System.out.println("5. Exit");
			Scanner sc = new Scanner(System.in);
			try {
				int n = sc.nextInt();
				switch (n) {
				case 1:
					Statistics.basicStatistics();
					break; // moment c
				case 2:
					Statistics.windForce();
					break; // moment B
				case 3:
					Statistics.averageTemp();
					break; // moment B
				case 4:
					Statistics.standardDeviation();
					break; // moment B
				case 5:
					System.out.println("Goodbye!");
					run = false;
					sc.close();
					break;
				default:
					System.out.println("That was not a number between 1 and 5.");
					break;
				}
			} catch (Exception e) {
				System.out.println("That was not a number.");
				continue;
			}
		}
	}

	public static void main(String[] args) {
		// Weather_analysis newAnalysis = new Weather_analysis();
		Weather_analysis.startAnalysis();
	}
}
