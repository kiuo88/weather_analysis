package weather_analysis;

/*
 * Constructor for measurement
 */
public class Measurment {
	String time;
	float temp, wind;
	int year, month;

	public Measurment(String xx, int aa, int bb, float yy, float zz) {
		time = xx;
		year = aa;
		month = bb;
		temp = yy;
		wind = zz;
	}

	public String getTime() {
		return time;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public float getTemp() {
		return temp;
	}

	public float getWind() {
		return wind;
	}

}
