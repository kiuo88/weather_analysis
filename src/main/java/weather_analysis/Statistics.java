package weather_analysis;

/*
 * this class does all the math
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

public class Statistics {
	static ArrayList<Measurment> measurementList = Weather_analysis.getList();

	public static void basicStatistics() {
		System.out.println("You selected 1\n");
		float minTemp = 100;
		float maxTemp = 0;
		float rTemp = 0;
		float averageTemp;
		for (int i = 0; i < measurementList.size(); i++) { // checks min max and
															// for ALL values in
															// list
			float currentTemp = measurementList.get(i).getTemp();
			if (currentTemp < minTemp) {
				minTemp = currentTemp;
			}
			if (currentTemp > maxTemp) {
				maxTemp = currentTemp;
			}
			rTemp += currentTemp;
		}
		averageTemp = rTemp / measurementList.size();
		System.out.println("Lowest Temperature is: " + minTemp + "�C");
		System.out.println("Highest Temperature is: " + maxTemp + "�C");
		System.out.println("Average Temperature for all data is: " + averageTemp + "�C\n");

	}

	public static void windForce() {
		System.out.println("You selected 2\n");
		float maxWind = 0.00f;
		String maxWindT = "";
		for (int i = 0; i < measurementList.size(); i++) { // checks max wind
															// and the time it
															// happened
			float currentWind = measurementList.get(i).getWind();
			if (currentWind > maxWind) {
				maxWind = currentWind;
				maxWindT = measurementList.get(i).getTime();
			}
		}
		System.out.println("Max wind speed was " + maxWind + " m/s at" + maxWindT + "\n");
	}

	public static void averageTemp() {
		System.out.println("You selected 3\n");
		int lastYear = 0;
		float tempSum = 0;
		int checkNum = 0;
		ArrayList<Integer> monthArraylist = new ArrayList<Integer>();
		Collection<Integer> months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);

		for (int i = 0; i < measurementList.size(); i++) { // get average for
															// each year
			int currentYear = measurementList.get(i).getYear();
			int currentMonth = measurementList.get(i).getMonth();

			if (currentYear != lastYear) {
				boolean allmonths = monthArraylist.containsAll(months);
				if (lastYear == 0) {
					System.out.println("Here is your data: \n");
				} else {
					if (allmonths) { // checks if the year has all 12 months
						System.out.println("Year " + lastYear + " average temperature was " + (tempSum / checkNum));
					} else {
						System.out.println("Year " + lastYear + " data was not complete");
					}
				}
				monthArraylist.clear();
				monthArraylist.add(currentMonth);
				tempSum = 0;
				tempSum += measurementList.get(i).getTemp();
				checkNum = 0;
				checkNum++;
			} else {
				boolean monthMatch = monthArraylist.contains(currentMonth);
				if (!monthMatch) {
					monthArraylist.add(currentMonth);
				}
				tempSum += measurementList.get(i).getTemp();
				checkNum++;
			}
			lastYear = currentYear;
		}
		System.out.println();
	}

	public static void standardDeviation() {
		ArrayList<Integer> yearArraylist = new ArrayList<Integer>();
		ArrayList<Integer> monthArraylist = new ArrayList<Integer>();
		Collection<Integer> months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
		int lastYear = 0;
		System.out.println("You selected 4\n");
		for (int i = 0; i < measurementList.size(); i++) { // Builds arrays of
															// months and years
			int currentYear = measurementList.get(i).getYear();
			int currentMonth = measurementList.get(i).getMonth();
			boolean allmonths = monthArraylist.containsAll(months);
			boolean monthMatch = monthArraylist.contains(currentMonth);
			if (lastYear != 0) {
				if (currentYear != lastYear) {
					if (allmonths) {
						yearArraylist.add(lastYear);
					}
					monthArraylist.clear();
					monthArraylist.add(currentMonth);
				} else {
					if (!monthMatch) {
						monthArraylist.add(currentMonth);
					}
				}
			}
			lastYear = currentYear;
		}
		boolean run = true;
		System.out.println("Choose a year from the following list: ");
		int inYear = 0;
		while (run) { // runs until user types a correct year
			System.out.println(Arrays.toString(yearArraylist.toArray()));
			System.out.println();
			Scanner sc = new Scanner(System.in);

			try {
				inYear = sc.nextInt();

			} catch (Exception e) {
				System.out.println("Not an integer!");
				continue;
			}
			if (!yearArraylist.contains(inYear)) {
				System.out.println("That was not in the list!\nChoose again.");
			} else {

				break;
			}
			sc.close();
		}
		String[] monthNames = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		float[] monthlyTemptemp = new float[12];

		int[] checkArray = new int[12];
		for (int i = 0; i < measurementList.size(); i++) { // builds monthly
															// temperature and
															// counts data per
															// month
			int currentYear = measurementList.get(i).getYear();
			int currentMonth = measurementList.get(i).getMonth();
			if (currentYear == inYear) // only work if current year is input
										// year
			{

				checkArray[currentMonth - 1]++;
				monthlyTemptemp[currentMonth - 1] += measurementList.get(i).getTemp();
			}

		}

		for (int i = 0; i < 12; i++) { // go through each month and build the
										// monthly data
			float quadsum = 0;
			float monthlyAverage = monthlyTemptemp[i] / checkArray[i];
			for (int j = 0; j < measurementList.size(); j++) { // create mean
																// value
				int currentYear = measurementList.get(j).getYear();
				int currentMonth = measurementList.get(j).getMonth();
				float currentTemp = measurementList.get(j).getTemp();
				if ((currentYear == inYear) && (currentMonth == (i + 1))) {
					quadsum += (currentTemp - monthlyAverage) * (currentTemp - monthlyAverage);

				}
			}
			float sum = quadsum / checkArray[i];
			System.out.println(monthNames[i] + ": " + Math.sqrt(sum));
			quadsum = 0;
		}
		System.out.println();
	}
}
